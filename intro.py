#namespace
'''
if i import math and numpy module at a same time i want to use sqrt method from both module i can, but 
inside python create new saprate namespace one for math and another for numpy where namespace is container of module code just like copy
second things python checks the whole code of module and third create name of the namespace
'''


#excersice 1 about modules
'''
#from math import pi #import only perticuler object
import math as ma
print(ma.sqrt(100)) # sqrt is method of math module
print(ma.pi)
'''

#exersice 2 about modules
'''
import math
import numpy

print(math.sqrt(3))
print(numpy.sqrt(3))

l1 = [1,4,9,16,25,49,64,81,100]

print(numpy.sqrt(l1))  # it is possible in numpy 
print(math.sqrt(l1))  # it is not possible in math modules with sqrt method
'''

#excersice 3 about find
'''
# ways to find how and which method is available for object
print(dir('smit'))  # dir method return list of inbuilt method for string 'smit'

# if i want to get description of any method we can using help method
print(help('smit'.upper))  # warning : dont use parenthecis
'''

#excersice 4 about calculation
# basic calculation
'''
a = 10
b = 20
c = 100

print(a+b)
print(a-b)
print(a*b)
print(a**b)  # no limation for number in sence how it`s long
print(a/b)
print(a//b)

# _ 'underscore' is operator to get last operation's integer value
# run below code in jupyter notebook

#10 // 7
#_
#_ + 10
'''

#excersice 5 about == vs is
'''
x = [1,2]
y = [1,2]
print(x == y) # it will give you true becasue '==' operator check the value is it same or not?
print(x is x) # it will give you false beacause 'is' keyword check the object of x and y. it belonges to same object or not 

x = [1,2]
y = [2,1]
ans = set(x) == set(y)  

print(ans)
'''

#execersice 6 about slicing
'''
t1 = (1,2,3)[-0]
print(t1)

t2 = (1,2,3)[-0:0]
print(t2)
'''

#excersice 7 about lists
# identical terms of lists : mutable , ordered list , having any kind of objects , sign : []

#excersice 8 about tuples
# identical terms of tuples : ordered list, having any kind of data , sign : ()
# use for packing and inpacking the data
'''
x = 12.12
y = 13.13
tup = (x,y)
print('Packing data into tuple togather : ',tup)

(x,y) = tup
print('Unpacking the data : ',end='')
print('x:',x,end=', y:')
print(y)
'''
'''
l1 = [
    (1,2),
    (2,3),
    (3,4)
]

for (x,y) in l1: # or we can also use `for x,y in l1:`
    print(x,y)
'''
'''
tup = (1,2)
print(tup)

tup = (1)
print(type(tup)) # it will give you type as a integer

tup = (1,)
print(type(tup)) # now this is tuple for showing one element in tuple and another way to code tup = 1,
'''

# excersice9 about strig
'''
string = 'Hello world'
print(string.replace('Hello','hiii'))
print(string)

# tricks : str.replace? (for searching about replace method)

string = '500000'
print(string.isdigit())  # check the string that is having digit or not
'''


#excersice10 about sets
'''
there are two types of sets one is normal sets and it is mutable and another is frozen sets which is not mutable(immutable)
second thing is sets are not indexed
third thing is sets are having distinct object
denoted by : {}
application : to perform mathamatics task like setoperation : union,inner,outer 
'''
'''
s1 = {1,2,3,4,5}
print(s1)
s1.add(6)
print(s1)
s1.add(6) # i am adding another 6 into the sets but nothing will be add beacuse sets removes all duplicate values
print(s1)

ids = set(range(10))
males = set([1,3,5,7,9])

females = ids - males
print(females)

everyone = males | females # union operation
print('Union operation:',everyone)

everyone = males & set(range(5)) # intersaction operation
print('Intersaction operation:',everyone)

s1 = 'axbsdfshvfdxffdfgbfgngfb'
print('Unique number in string : ',len(set(s1)))

x = {1,2,3}
y = {1,2,3,4}
print(x.issubset(y))
print(y.issuperset(x))
'''

# excersice11 about dictionary
'''
dictionary having key-value pair where key is immutable and value is anything 
dictionary is not in order like left to right or right to left 
'''
'''
d1 = dict() # create empty dictionary
d2 = {} # another way
print(type(d1))
print(type(d2))

age = {
    'smit' : 19,
    'ankit' : 20,
    'shailesh' : 22
}

print(age['ankit'])
age['ankit'] += 1
print(age['ankit'])

age['Khalid'] = 40  # we can add any key value pair dynamically
print(age)

print(age.keys())
print(age.values())
print(age.items())

d1 = {
    'string' : 1,
    [1,2] : 2,  # error is occur because we cant use list as a key
    (1,2) : 3
}
print(d1)
'''