# excersice1 about dynamic typing
'''
python is dynamic typed langauge means python uderstand types of data at run-time
'''
'''
L = [1,2,3]
M = [1,2,3]

print(L == M) # L`s 0th element compare with M`s 0th element and so on
print(L is M) # hear L and M is having diffrent object as a refrence


# L(varible) ---> [1,2,3](object)
# M(varible) ---> [1,2,3](object)
  
print(id(L)) 
print(id(M))
'''
'''
L = [1,2,3]
M = [1,2,3]

L = M
# L ---> [1,2,3] now both having same reference
# M ------^
print(L == M)
print(L is M)
'''
'''
# crete copy of list
L = [1,2]
M = [3,4]
L = list(M)
print(L is M)

L = [1,2]
M = [3,4]
L = M[:]
print(L is M)
'''

#excersice2 about copies
'''
Shallow copy and deep copy
'''
'''
#shallow copy 
import copy
x = [1,2]

# y ---> x' (x' copy of x with 1 and 2)
y = copy.copy(x)  #shallow copy : this will crearte copy of x than refers to the object of x' with 1,2
print(x == y)
print(x is y)
# z ---> x'' (x'' copy of x with copy of 1',2' where 1 and 2 is value of object list)
z = copy.deepcopy(x) #deep copy : this will create copy of x than also creates copy of x`s objects and than refers to the x'' with 1',2'
print(x == z) 
print(x is z) 

print(y == z)
print(y is z) # x' is x'' that's why it's return false 
'''

'''extra'''
'''
import getpass
passw = int(getpass.getpass('Enter password : '))
if passw == 123456:
    print('succesfully logIn')
'''

#excersice3 about for loop
'''
data = {'smit': 25, 'ravan': 50, 'shailesh': 23, 'ankit': 40, 'savan': 34, 'bhavesh': 21}

#iterate the data using for loop
for k,v in data.items():
    print(k,v)
print('\nsecond way : ')
for k in data:
    print(k,data[k])
print('\nthird way : ')
for k in data.keys():
    print(k,data[k])

#some addition
print('\nsorted data : ')
for k in sorted(data):
    print(k,data[k])

print('\nsorted with reverse : ')
for k in sorted(data,reverse=True):
    print(k,data[k])

l1 = range(10)
l2 = [x**2 for x in l1]  # this things called as list compherasion
print(l2)

l3 = [x for x in range(0,9) if x%2==0] # for loop with condition
print(l3)
'''

#excersice4 about FILE handling
'''
#way to read exsting file
file_name = 'intro.py'
for content in open(file_name):  # also read d=file like this this will be happen because open function retun iterator object of file
    print(content,end='') # print(content.rstrip()) hear rstrip method removes extra subsequent charcater to both sides of the string

#first mode is 'w' thats indicates to create file if not exits otherwise create new file
f = open('newfile.txt','w')
f.write('Python File handling')
f.close()
'''

# excersice5 about function 
'''
def mutliply(myList):
    myList[0] *= 10

myList =[1,2,3,4,5]

mutliply(myList)

print(myList)
'''